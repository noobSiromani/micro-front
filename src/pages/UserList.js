import { useEffect, useState } from "react";

import {Link} from "react-router-dom";

import axios from '../utils/axios'

function UserList(){
    const [users, setUsers] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        fetchUsers()
    }, [])

    function fetchUsers(){
        setLoading(true)
        axios.get('/users')
        .then(res => {
            const {users} = res.data

            setUsers(users)
        })
        .catch(err => console.log(err))
        .finally(() => setLoading(false))
    }

    function exportToCsv(){
        let items = [
            {name: '', email: '', gender: '', status: ''},
            ...users.map(({name, email, gender, status}) => ({name, email, gender, status}))
        ]

        let csv = '';

        for(let row = 0; row < items.length; row++){
            let keysAmount = Object.keys(items[row]).length
            let keysCounter = 0
        
            // If this is the first row, generate the headings
            if(row === 0){
        
               // Loop each property of the object
               for(let key in items[row]){
        
                                   // This is to not add a comma at the last cell
                                   // The '\r\n' adds a new line
                   csv += key + (keysCounter+1 < keysAmount ? ',' : '\r\n' )
                   keysCounter++
               }
            }else{
               for(let key in items[row]){
                   csv += items[row][key] + (keysCounter+1 < keysAmount ? ',' : '\r\n' )
                   keysCounter++
               }
            }
        
            keysCounter = 0
        }
        console.log(csv)
        // Once we are done looping, download the .csv by creating a link
        let link = document.createElement('a')
        link.id = 'download-csv'
        link.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(csv));
        link.setAttribute('download', 'users.csv');
        document.body.appendChild(link)
        document.querySelector('#download-csv').click()
    }

    if(loading){
        return (
            <div>
                <span>Please wait ...</span>
            </div>
        )
    }

    return (
        <div>
            <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
                <h3>User List</h3>
                <button style={{height: '20px'}} onClick={exportToCsv}>Export</button>
            </div>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Gender</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {users.map(({id, name, email, gender, status}) => (
                        <tr>
                            <td>{name}</td>
                            <td>{email}</td>
                            <td>{gender}</td>
                            <td>{status}</td>
                            <td>
                                <Link to={`/${id}`}>Edit</Link>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default UserList