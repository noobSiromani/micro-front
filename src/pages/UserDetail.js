import {useParams} from "react-router-dom";
import { useEffect, useState } from "react";
import axios from '../utils/axios'

function UserDetail(){
    const messageDuration = 2000
    const {id} = useParams();
    const [user, setUser] = useState(null)
    const [loading, setLoading] = useState(false)
    const [showMessage, setShowMessage] = useState(false)
    const [message, setMessage] = useState('')

    useEffect(() => {
        fetchUser()
    }, [])

    function fetchUser(){
        if(!id) return;
        setLoading(true)
        axios.get(`/users/${id}`)
        .then(res => {
            const {user} = res.data
            setUser({
                ...user,
                status: user.status === 'active' ? true : false
            })
        })
        .catch(err => console.log(err))
        .finally(() => {
            setLoading(false)
        })
    }

    function updateUser(ev){
        ev.preventDefault()
        axios.put(`/users/${id}`, {
            ...user,
            status: user.status ? 'active' : 'inactive'
        })
        .then(res => {
            if(res.status === 200){
                alert("User updated successfully")
            }else{
                alert(res.data.message || "Something went wrong")
            }
        })
        .catch(err => console.log(err))
        .finally(() => {

        })
    }

    function alert(message){
        setMessage(message)
        setShowMessage(true)
    
        setTimeout(() => {
          setShowMessage(false)
          setMessage('')
        }, messageDuration)
    }

    if(loading){
        return(
            <div>
                <span>Please wait ...</span>
            </div>
        )
    }

    if(!user){
        return(
            <div>
                <span>User not found</span>
            </div>
        )
    }

    return (
        <div>
            <h3>User Detail</h3>
            <div style={{display: 'flex', justifyContent: 'flex-start'}}>
            <form onSubmit={updateUser}>
                <div>
                    <label htmlFor="name">Name</label><br/>
                    <input type="text" name="name" id="name" value={user.name} 
                    onChange={e => setUser(user => ({...user, name: e.target.value}))}/>
                </div>
                
                <div>
                    <label htmlFor="email">Email</label><br/>
                    <input type="email" name="email" id="email" value={user.email} 
                    onChange={e => setUser(user => ({...user, email: e.target.value}))}/>
                </div>

                <div>
                    <p>Gender</p>
                    <div>
                        <input type="radio" id="gender-male" name="gender" 
                        value="male" checked={user.gender === "male"} 
                        onChange={e => setUser(user => ({...user, gender: e.target.value}))}
                        />
                        <label for="gender-male">Male</label>
                        <input type="radio" id="gender-female" name="gender" 
                        value="female" checked={user.gender === "female"} 
                        onChange={e => setUser(user => ({...user, gender: e.target.value}))}
                        />
                        <label for="gender-female">Female</label>
                    </div>
                </div>
                
                <div>
                    <label htmlFor="status">Status</label>
                    <input type="checkbox" name="status" id="status" checked={user.status} 
                    onChange={() => setUser(user => ({...user, status: !user.status}))}/>
                </div>

                <div>
                    <button className="expanded-btn" type="submit">Update</button>
                </div>
            </form>
            </div>
            
            {showMessage && <Message message={message} />}
        </div>
    )
}

function Message(props){
    const {message = "Something went wrong"} = props
  
    return (
      <div className='message'>
        <span>{message}</span>
      </div>
    )
}

export default UserDetail