import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom"

import UserList from './pages/UserList'
import UserDetail from './pages/UserDetail'

import './App.css';

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route exact path="/" element={<UserList />} />
          <Route path="/:id" element={<UserDetail />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
